+++
title = "Demo Servers"
+++
## Demo servers
The demo servers are meant to showcase the latest developments of the LibreHealth project. Each project should add a link to their demo site through this page.

## LibreHealth Toolkit LTS 1.12.0 demo
LibreHealth Toolkit master demo is a deployment that is done by GitLab CI after every commit.

 - URL - https://toolkit.librehealth.io/1.12.x
 - Username: admin
 - Password: Hello@123

## LibreHealth Toolkit LTS 2.0.0 demo
LibreHealth Toolkit LTS 2.0.0 demo is a deployment that is done by GitLab CI after every commit.

 - URL - https://toolkit.librehealth.io/master
 - Username: admin
 - Password: Hello@123

## LibreHealth EHR demo
LibreHealth EHR demo is based on the latest stable release. This contains no real data.

 - URL - https://ehr.librehealth.io
 - Username: admin
 - Pass Phrase: password

## LibreHealth EHR w/ The National Health and Nutrition Examination Survey (NHANES) data

An instance of LibreHealth EHR has been deployed with data from NHANES. You can read more about it [on our forum](https://forums.librehealth.io/t/nhanes-data-in-libreehr/735).

You can also download the data [here](https://ehr.librehealth.io/downloads/nhanes) for local use.

- URL: https://ehr.librehealth.io/nhanes
- Username: admin
- Pass Phrase: password
